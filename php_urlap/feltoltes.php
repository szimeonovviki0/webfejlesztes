<?php
	$header_title = "Tabla feltoltese";
	include 'includes/overall/header.php'; 
	if(isset($_POST["submit"]))
	{
		if(!isset($_POST["tabla"]) || empty ($_POST["tabla"]))
		{
			$tablak["tabla"]="Nincs kitoltve az adat";
		}
		else
		{
			$tabla=$_POST["tabla"];
			$tabla=serialize1($_POST["tabla"]);
			if(!preg_match("/^[a-zA-Z]+$/", $tabla))
			{
				$tablak["tabla"]="Csak kis-, nagybetuket, illetve _ tartalmazhat";
			}
		}
	}
	
	//tabla letrehozasa
	include 'core/database/conn.php';
	$sql2 = "CREATE TABLE (
		vNev VARCHAR(30) NOT NULL,
		kNev VARCHAR(30) NOT NULL,
		tel VARCHAR(12) NOT NULL,
		email VARCHAR(30) NOT NULL,
	)";
	$result2 = $conn -> query($sql2);
	
?>
<section>
	<!--<h3>Szerkesztes alatt</h3>-->
	<form action = "" method = "post">

			<label>Tábla neve</label><input type = "text"  name = "vNev"   >
			<div class = "hiba"> 
				<?php 
					if(isset($hiba["vNev"])) echo $hiba["vNev"];
				?> 
			</div>
			<div style="float: left; margin: 10px 20px;">
				<input type = "submit" name = "submit" value = "Mentés">
			</div>

	</form>
</section>
<?php
	include 'includes/overall/footer.php'; 
?>