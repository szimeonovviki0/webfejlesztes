<?php	
$hiba=array();
function ellenorzes()
{
	if(isset($_POST["submit"]))
	{
		if(!isset($_POST["vNev"]) || empty ($_POST["vNev"]))
		{
			$hiba["vNev"]="Nincs kitoltve az adat";
		}
		else
		{
			$vNev=$_POST["vNev"];
			$vNev=serialize1($_POST["vNev"]);
			if(!preg_match("/^[a-zA-Z]+$/", $vNev))
			{
				$hiba["vNev"]="Csak betuket tartalmazhat";
			}
		}
		
		
		if(!isset($_POST["kNev"]) || empty ($_POST["kNev"]))
		{
			$hiba["kNev"]="Nincs kitoltve az adat";
		}
		else
		{
			$kNev=$_POST["kNev"];
			$kNev=serialize1($_POST["kNev"]);
			if(!preg_match("/^[a-zA-Z]+$/", $vNev))
			{
				$hiba["kNev"]="Csak betuket tartalmazhat";
			}
		}
		
		
		if(!isset($_POST["tel"]) || empty ($_POST["tel"]))
		{
			$hiba["tel"]="Nincs kitoltve az adat";
		}
		else
		{
			$tel=$_POST["tel"];
			$tel=serialize1($_POST["tel"]);
			if(!preg_match("/^[0-9]{10}|\+[0-9]{12}$/", $tel))
			{
				$hiba["tel"]="Csak szamokat tartalmazhat";
			}
		}

		
		if(!isset($_POST["email"]) || empty ($_POST["email"]))
		{
			$hiba["email"]="Nincs kitoltve az adat";
		}
		else
		{
			$email=$_POST["email"];
			$email=serialize1($_POST["email"]);
			if(!filter_var($_POST["email"],FILTER_VALIDATE_EMAIL))
			{
				$hiba["email"]="Nem jol van beirva az email";
			}
		}
	}
}
function serialize1($adat)
		{
			$adat=trim($adat);
			$adat=stripslashes($adat);
			$adat=htmlspecialchars($adat);
			return $adat;
		}
?>